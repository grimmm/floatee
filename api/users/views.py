from django.shortcuts import render
from django.contrib.auth import get_user_model  # If used custom user model
from allauth.socialaccount.providers.facebook.views import FacebookOAuth2Adapter
from allauth.socialaccount.providers.google.views import GoogleOAuth2Adapter
from rest_auth.registration.views import SocialLoginView
from rest_framework import viewsets, permissions
from django.conf import settings
from rest_auth.utils import jwt_encode
from rest_auth.app_settings import (TokenSerializer,
                                    JWTSerializer,
                                    create_token)
from allauth.account import app_settings as allauth_settings
from allauth.account.utils import complete_signup


from .models import User, Account
from view_sets import UpdateRetrieveViewSet, SoftDeleteModelViewSet
from .serializers import UserSerializer, AccountSerializer


class FacebookLogin(SocialLoginView):
    adapter_class = FacebookOAuth2Adapter


class GoogleLogin(SocialLoginView):
    adapter_class = GoogleOAuth2Adapter


class AccountViewSet(SoftDeleteModelViewSet):
    serializer_class = AccountSerializer
    queryset = Account.objects.all()
    lookup_field = "external_id"
