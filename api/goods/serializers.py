from rest_framework import serializers
from .models import Good


class GoodSerializer(serializers.ModelSerializer):
    class Meta:
        model = Good
        fields = [
            "external_id",
            "name",
            "sku",
            "account",
            "is_deleted",
            "created_at",
            "updated_at",
            "deleted_at",
        ]

