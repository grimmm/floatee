from rest_framework import serializers
from .models import Gift, Offering
from donors.models import Donation
from goods.models import Good


class GiftSerializer(serializers.ModelSerializer):
    offering = serializers.SlugRelatedField(
        slug_field="external_id", queryset=Offering.objects.all()
    )
    donation = serializers.SlugRelatedField(
        slug_field="external_id", queryset=Donation.objects.all()
    )

    class Meta:
        model = Gift
        fields = [
            "external_id",
            "donation",
            "offering",
            "created_at",
            "updated_at",
            "deleted_at",
            "is_deleted",
        ]


class OfferingSerializer(serializers.ModelSerializer):
    goods = serializers.SlugRelatedField(slug_field="external_id", many=True, queryset=Good.objects.all())

    class Meta:
        model = Gift
        fields = [
            "external_id",
            "goods",
            "threshold",
            "created_at",
            "updated_at",
            "deleted_at",
            "is_deleted",
        ]
