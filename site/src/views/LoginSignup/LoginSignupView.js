import React, { useState, useEffect } from "react";
import { makeStyles } from "@material-ui/core/styles";
import GridItem from "components/Grid/GridItem.js";
import GridContainer from "components/Grid/GridContainer.js";
import { CardContent, CardActions } from "@material-ui/core";
import Card from "components/Card/Card.js";
import CardHeader from "components/Card/CardHeader.js";
import CardBody from "components/Card/CardBody.js";
import CardFooter from '../../components/Card/CardFooter';
import Button from "@material-ui/core/Button";
import FormControl from '@material-ui/core/FormControl';
import FormGroup from '@material-ui/core/FormGroup';
import FormHelperText from '@material-ui/core/FormHelperText';
import { ValidatorForm, TextValidator, SelectValidator } from 'react-material-ui-form-validator';

import bgImage from "assets/img/auth-bg.jpg";

import styles from "assets/jss/material-dashboard-react/views/authStyle.js";
import request from '../../lib/requestClient';

const useStyles = makeStyles(styles);

function SignInForm(props) {
  const { classes, toggleSignIn } = props;
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");

  return (
    <ValidatorForm onSubmit={() => null} className={classes.authForm}>
      <CardHeader color="warning" subheader="Sign in or sign up!">
        <div className={classes.typo}>
          <h2>Welcome to Floatee!</h2>
        </div>
      </CardHeader>
      <CardBody>
        <CardContent>
          <FormControl component="fieldset" className={classes.authFormControl}>
            <FormGroup>
              <TextValidator
                label="Enter your email"
                onChange={(event) => setEmail(event.target.value)}
                name="email"
                value={email}
                required
                fullWidth
                validators={['required', 'isEmail']}
                errorMessages={['this field is required', 'email is not valid']}
              />
            </FormGroup>
          </FormControl>
          <FormControl component="fieldset" className={classes.authFormControl}>
            <FormGroup>
              <TextValidator
                label="Enter your password"
                onChange={(event) => setPassword(event.target.value)}
                name="password"
                value={password}
                required
                type="password"
                fullWidth
                validators={['required']}
                errorMessages={['this field is required']}
              />
            </FormGroup>
          </FormControl>
        </CardContent>
        <FormControl component="fieldset" className={classes.authFormControl}>
          <FormGroup>
            <CardActions style={{ justifyContent: "space-between" }}>
              <Button type="submit" color="primary" raised="true">
                Sign in
              </Button>
              <Button>Forgot password</Button>
            </CardActions>
          </FormGroup>
        </FormControl>
      </CardBody>
      <CardFooter className={classes.signInFooter}>
        Don't have an account? <Button onClick={() => toggleSignIn(false)}>Sign up!</Button>
      </CardFooter>
    </ValidatorForm>
  )
}



function SignUpForm(props) {
  const { classes, toggleSignIn } = props;
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  const [passmatch, setPassmatch] = useState("");
  const [isOrg, setIsOrg] = useState(false);
  const [orgName, setOrgName] = useState("");

  const submitSignUp = async (event) => {
    event.preventDefault();
    if (isOrg !== "true") {
      orgName = email;
    }

    const accountResp = await request.call("post", `${process.env.REACT_APP_API_SERVER}/account/`)
      .type("application/json")
      .send({
        organization_name: orgName,
        is_recipient: isOrg === "true",
        account_details: {}
      });

    const resp = await request.call("post", `${process.env.REACT_APP_API_SERVER}/auth/registration/`)
      .type('application/json')
      .send({
        email,
        password1: password,
        password2: passmatch,
        account_external_id: accountResp.body.external_id
      })
    debugger;
  }

  useEffect(() => {
    ValidatorForm.addValidationRule('isPasswordMatch', (value) => {
      if (value !== password) {
        return false;
      }
      return true;
    });
    ValidatorForm.addValidationRule("meetsPasswordRequirements", (value) => {
      var strongRegex = new RegExp("^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[!@#\$%\^&\*])(?=.{12,})");

      return strongRegex.test(value);
    })
  }, [password]);

  return (
    <ValidatorForm onSubmit={submitSignUp} className={classes.authForm}>
      <CardHeader color="warning" subheader="Sign in or sign up!">
        <div className={classes.typo}>
          <h2>Welcome to Floatee!</h2>
        </div>
      </CardHeader>
      <CardBody>
        <CardContent>
          <FormControl component="fieldset" className={classes.authFormControl}>
            <FormGroup>
              <TextValidator
                label="Enter your email"
                onChange={(event) => setEmail(event.target.value)}
                name="email"
                value={email}
                required
                fullWidth
                validators={['required', 'isEmail']}
                errorMessages={['this field is required', 'email is not valid']}
              />
            </FormGroup>
          </FormControl>
          <FormControl component="fieldset" className={classes.authFormControl}>
            <FormGroup>
              <TextValidator
                label="Enter your password"
                onChange={(event) => setPassword(event.target.value)}
                name="password"
                value={password}
                type="password"
                required
                fullWidth
                validators={['required', 'meetsPasswordRequirements']}
                errorMessages={['this field is required', 'password is not valid']}
              />
              <TextValidator
                label="Re-enter your password"
                onChange={(event) => setPassmatch(event.target.value)}
                name="password_match"
                type="password"
                value={passmatch}
                required
                fullWidth
                validators={['required', 'isPasswordMatch']}
                errorMessages={['this field is required', 'password does not match']}
              />
            </FormGroup>
          </FormControl>
          <FormControl component="fieldset" className={classes.authFormControl}>
            <SelectValidator
              value={isOrg}
              onChange={(event) => setIsOrg(event.target.value)}
              SelectProps={{
                native: true,
                name: 'recipient-account-check',
                id: 'recipient-account-check-helper',
              }}
            >
              <option value={false}>No</option>
              <option value={true}>Yes</option>
            </SelectValidator>
            <FormHelperText>Is this account for a business seeking donations?</FormHelperText>
          </FormControl>
          {isOrg === 'true' ? (<OrgFormFieldGroup orgName={orgName} classes={classes} setOrgName={(event) => setOrgName(event.target.value)} />) : null}
        </CardContent>
        <CardActions style={{ justifyContent: "center" }}>
          <Button type="submit" color="primary" raised="true">
            Sign up!
          </Button>
        </CardActions>
      </CardBody>
      <CardFooter>
        Already have an account? <Button onClick={() => toggleSignIn(true)}>Sign in!</Button>
      </CardFooter>
    </ValidatorForm>
  )
}

function OrgFormFieldGroup(props) {
  const { classes, orgName, setOrgName } = props;
  return (
    <React.Fragment>
      <FormControl component="fieldset" className={classes.authFormControl}>
        <FormGroup>
          <TextValidator
            label="Enter your Organization name"
            onChange={setOrgName}
            name="org_name"
            value={orgName}
            required
            fullWidth
            validators={['required']}
            errorMessages={['this field is required']}
          />
        </FormGroup>
      </FormControl>
    </React.Fragment>
  )
}

export default function LoginSignup() {
  const classes = useStyles();
  const [isSignIn, toggleSignIn] = useState(true)
  return (
    <div className={classes.authContainer}>
      <GridContainer className={classes.authGridContainer}>
        <GridItem xs={12} sm={8} md={3}>
          <Card chart>
            {isSignIn ? (<SignInForm classes={classes} toggleSignIn={toggleSignIn} />) : (<SignUpForm classes={classes} toggleSignIn={toggleSignIn} />)}
          </Card>
        </GridItem>
      </GridContainer>
      {bgImage !== undefined ? (
        <div
          className={classes.background}
          style={{ backgroundImage: "url(" + bgImage + ")" }}
        />
      ) : null}
    </div>
  );
}
