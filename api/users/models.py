import uuid

from django.contrib.auth.models import AbstractUser, BaseUserManager
from django.db import models
from django.utils.translation import ugettext_lazy
from django.contrib.postgres.fields import JSONField
from enum import Enum


class UserManager(BaseUserManager):
    def __init__(self):
        super().__init__()

    def create_user(self, email, password, account_external_id, **extra_fields):
        account = Account.objects.get(external_id=account_external_id)

        email = self.normalize_email(email)
        user = self.model(email=email, **extra_fields)
        user.account = account
        user.set_password(password)
        user.save()
        return user

    def create_superuser(self, email, password):
        try:
            admin_account = Account.objects.get(organization_name="Floatee Inc.")
        except Account.DoesNotExist as e:
            admin_account = self.create_account(
                "Floatee Inc.", is_recipient=True, details={}
            )
        user = self.create_user(
            email,
            password=password,
            account_external_id=admin_account.external_id,
            is_staff=True,
            is_superuser=True,
        )
        return user

    def create_account(self, organization_name, is_recipient, details={}):
        account = Account(organization_name=organization_name)
        account.save()

        account_details = AccountDetails(
            account=account, details=details, is_recipient=is_recipient
        )
        account_details.save()


class Account(models.Model):
    external_id = models.UUIDField(default=uuid.uuid4, editable=False, unique=True)
    organization_name = models.CharField(max_length=1024)
    created_at = models.DateTimeField(auto_now=True)
    updated_at = models.DateTimeField(auto_now=True)


class AccountDetails(models.Model):
    is_recipient = models.BooleanField(default=False)
    external_id = models.UUIDField(default=uuid.uuid4, editable=False, unique=True)
    details = JSONField()
    account = models.ForeignKey(
        Account, related_name="account_details", on_delete=models.CASCADE
    )
    created_at = models.DateTimeField(auto_now=True)
    updated_at = models.DateTimeField(auto_now=True)
    deleted_at = models.DateTimeField(default=None, null=True)
    is_deleted = models.BooleanField(default=False, null=False)


class User(AbstractUser):
    external_id = models.UUIDField(default=uuid.uuid4, editable=False, unique=True)
    username = None
    account = models.ForeignKey(Account, related_name="user", on_delete=models.PROTECT)
    email = models.EmailField(ugettext_lazy("email address"), unique=True)

    objects = UserManager()
    USERNAME_FIELD = "email"
    REQUIRED_FIELDS = []

    def __str__(self):
        return str(self.external_id)

    created_at = models.DateTimeField(auto_now=True)
    updated_at = models.DateTimeField(auto_now=True)
    deleted_at = models.DateTimeField(default=None, null=True)
    is_deleted = models.BooleanField(default=False, null=False)


# This seems to be a rare case where customizing both the user and the useradmin
# in the same file requires the custom user to first be defined before importing
# the BaseUserAdmin as the BaseUserAdmin internally references the user model set
# in AUTH_USER_MODEL in api/settings.py
from django.contrib.auth.admin import UserAdmin as BaseUserAdmin


class UserAdmin(BaseUserAdmin):
    ordering = ("email",)
