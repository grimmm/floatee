from django.shortcuts import render
from rest_framework import viewsets
from .models import Good
from .serializers import GoodSerializer

# Create your views here.


class GoodsViewSet(viewsets.ModelViewSet):
    lookup_field = "external_id"
    queryset = Good.objects.all()
    serializer = GoodSerializer
