import uuid

from django.db import models
from donors.models import Donation
from goods.models import Good


class Offering(models.Model):
    """
    An Offering is the configuration for a Good a Recipient can give
    to a Donor in the form of a Gift. Recipients can set many offerings
    with different thresholds of donation amount required to recieve it.
    """

    external_id = models.UUIDField(default=uuid.uuid4, editable=False, unique=True)
    goods = models.ManyToManyField(Good)
    threshold = models.FloatField(null=False, default=1.00)
    created_at = models.DateTimeField(auto_now=True)
    updated_at = models.DateTimeField(auto_now=True)
    deleted_at = models.DateTimeField(default=None, null=True)
    is_deleted = models.BooleanField(default=False, null=False)

# Create your models here.
class Gift(models.Model):
    """
    Gifts are the record of a set of Goods given to Donors from Recipients
    when a threshold on an Offering has been met.
    They are an optional nicety to show Donors their donation
    was appreciated.
    """

    external_id = models.UUIDField
    donation = models.ForeignKey(Donation, on_delete=models.CASCADE)
    offering = models.ForeignKey(Offering, on_delete=models.CASCADE)
    created_at = models.DateTimeField(auto_now=True)
    updated_at = models.DateTimeField(auto_now=True)
    deleted_at = models.DateTimeField(default=None)
    is_deleted = models.BooleanField(default=False, null=False)
