import logging

from rest_framework import serializers
from django.contrib.auth import get_user_model  # If used custom user model
from allauth.account.adapter import get_adapter
from allauth.account.utils import setup_user_email
from .models import Account, AccountDetails
from rest_auth.registration.serializers import RegisterSerializer

# Get an instance of a logger
logger = logging.getLogger(__name__)

User = get_user_model()


class UserSerializer(serializers.Serializer):
    external_id = serializers.UUIDField(format="hex_verbose", read_only=True)
    email = serializers.EmailField(read_only=True)
    account = serializers.SlugRelatedField(
        slug_field="external_id", queryset=Account.objects.all()
    )

    class Meta:
        model = User

        fields = ["email", "external_id", "account"]

    # def update(self, instance, validated_data):
    #     role_data = validated_data.pop("roles")
    #     for role in role_data:
    #         Role.objects.create(user=instance, **role)

    #     instance.save()
    #     return instance


class AccountDetailsSerializer(serializers.ModelSerializer):
    class Meta:
        model = AccountDetails
        fields = ["external_id", "details", "is_recipient"]


class AccountSerializer(serializers.ModelSerializer):
    account_details = AccountDetailsSerializer(read_only=True, many=True)

    def create(self, validated_data):
        account = Account.objects.create(
            organization_name=validated_data["organization_name"]
        )
        details = AccountDetails.objects.create(
            is_recipient=self.context["request"].data["is_recipient"],
            details=self.context["request"].data["account_details"],
            account=account,
        )

        return account

    class Meta:
        model = Account
        fields = [
            "external_id",
            "organization_name",
            "account_details",
            "created_at",
            "updated_at",
        ]


class RegistrationSerializer(RegisterSerializer):
    def save(self, request):
        adapter = get_adapter()
        user = adapter.new_user(request)
        account = Account.objects.get(external_id=request.data["account_external_id"])
        user.account = account
        self.cleaned_data = self.get_cleaned_data()
        adapter.save_user(request, user, self)

        self.custom_signup(request, user)
        setup_user_email(request, user, [])

        return user
